#include <algorithm>
#include <cassert>
#include <cstdlib>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <random>
#include <set>
#include <string>
#include <unordered_set>
#include <utility>

#include "bound_set.h"
#include "digraph.h"
#include "q_node.h"
#include "scenario.h"

typedef enum {
  OPTIM_NONE = 0,
  OPTIM_LAZY = 1,
  OPTIM_TARJAN = 2,
  OPTIM_LAZY_TARJAN = 3,
} optim_t;

void init(int argc, char **argv, digraph &g, int &k, optim_t &optimization,
          std::vector<scenario> &scenarios);

double influence_gain(const std::vector<scenario> &scenarios, int v);
std::unordered_map<int, double>
influence_gains(const std::vector<scenario> &scenarios);
std::unordered_map<int, double>
influence_gains_slow(const std::vector<scenario> &scenarios);

template <typename Func>
std::unordered_set<int> solve_greedy(int k, digraph g,
                                     std::vector<scenario> &scenarios,
                                     double &influence, Func f_inf_gains);
template <typename Func>
std::unordered_set<int>
solve_greedy_lazy_forward(int k, digraph g, std::vector<scenario> &scenarios,
                          double &influence, Func f_inf_gains);
// std::unordered_set<int> solve_greedy_celf(int k, digraph g,
// std::vector<scenario> &scenarios);

double marginal_gain_wrt(std::vector<scenario> scenarios, std::vector<int> set,
                         int v);

int main(int argc, char **argv) {
  digraph g;
  int k;
  std::vector<scenario> scenarios;
  optim_t optimization = OPTIM_NONE;

  // parse parameters and generate scenarios
  init(argc, argv, g, k, optimization, scenarios);

  // solve with selected optimizations
  std::unordered_set<int> solution;
  double influence;

  clock_t begin = clock();

  switch (optimization) {
  case OPTIM_LAZY:
    solution = solve_greedy_lazy_forward(k, g, scenarios, influence,
                                         influence_gains_slow);
    break;
  case OPTIM_TARJAN:
    solution = solve_greedy(k, g, scenarios, influence, influence_gains);
    break;
  case OPTIM_LAZY_TARJAN:
    solution =
        solve_greedy_lazy_forward(k, g, scenarios, influence, influence_gains);
    break;
  case OPTIM_NONE:
  default:
    solution = solve_greedy(k, g, scenarios, influence, influence_gains_slow);
    break;
  }

  clock_t end = clock();
  double elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;
  std::cout << "Elapsed seconds: " << elapsed_secs << std::endl;

  std::set<int> sorted_solution(solution.begin(), solution.end());

  assert(solution.size() == (size_t)k);
  std::cout << "Influence: " << influence << std::endl;
  for (int v : sorted_solution)
    std::cout << v << std::endl;
  return 0;
}

template <typename Func>
std::unordered_set<int> solve_greedy(int k, digraph g,
                                     std::vector<scenario> &scenarios,
                                     double &influence, Func f_inf_gains) {
  std::unordered_set<int> A;
  influence = 0;
  for (int i = 0; i < k; i++) {
    int best_vertex = -1;
    double max_influence_gain = -1;
    std::unordered_map<int, double> inf_gains(f_inf_gains(scenarios));
    for (int v : g) {
      double gain = inf_gains[v];
      if (gain > max_influence_gain) {
        max_influence_gain = gain;
        best_vertex = v;
      }
    }
    assert(best_vertex != -1);
    A.insert(best_vertex);
    influence += max_influence_gain;
    for (scenario &s : scenarios)
      s.remove_reachable(best_vertex);
  }
  return A;
}

template <typename Func>
std::unordered_set<int>
solve_greedy_lazy_forward(int k, digraph g, std::vector<scenario> &scenarios,
                          double &influence, Func f_inf_gains) {
  std::unordered_set<int> S;
  std::vector<Q_node> Q;

  std::unordered_map<int, double> inf_gains(f_inf_gains(scenarios));
  for (int v : g) {
    Q_node u;
    u.vertex = v;
    u.mg1 = inf_gains[v];
    Q.push_back(u);
  }
  std::sort(Q.begin(), Q.end());

  influence = 0;
  while (S.size() < (size_t)k) {
    influence += Q.back().mg1;
    S.insert(Q.back().vertex);
    for (scenario &s : scenarios)
      s.remove_reachable(Q.back().vertex);
    Q.pop_back();

    double cur_best = 0;
    auto q = Q.rbegin();
    for (; q != Q.rend(); q++) {
      if (q->mg1 <= cur_best)
        break;
      q->mg1 = influence_gain(scenarios, q->vertex);
      cur_best = q->mg1;
    }
    std::sort(q.base(), Q.end());
  }
  return S;
}

double marginal_gain_wrt(std::vector<scenario> scenarios, std::vector<int> set,
                         int v) {
  for (int u : set)
    for (scenario &s : scenarios)
      s.remove_reachable(u);
  return influence_gain(scenarios, v);
}

void init(int argc, char **argv, digraph &g, int &k, optim_t &optimization,
          std::vector<scenario> &scenarios) {
  static std::map<int, int> vertex_to_id;
  static std::map<std::pair<int, int>, int> edge_to_id;

  // Parse parameters
  if (argc != 6) {
    std::cout << "Usage: " << argv[0]
              << " <k> <nr-scenarios> <p> <rand-seed> <optimization>"
              << std::endl;
    std::cout << "optimization:" << std::endl
              << "  0 = NO_OPTIMIZATION" << std::endl
              << "  1 = LAZY_FORWARD_EVALUATION" << std::endl
              << "  2 = TARJAN" << std::endl
              << "  3 = LAZY_FORWARD_EVALUATION+TARJAN" << std::endl;
    exit(1);
  }
  k = std::stoi(argv[1]);
  size_t nr_scenarios = std::stoi(argv[2]);
  double p = std::stod(argv[3]);
  int seed = std::stoi(argv[4]);
  optimization = (optim_t)std::stoi(argv[5]);

  // Read graph
  g = digraph(edges_from_stream(std::cin));

  // Set up 'id_to_edge' and 'edge_to_id'
  int i = 0;
  edge_to_id.clear();
  for (int v : g)
    for (int w : g.neighbors_out(v))
      edge_to_id[std::make_pair(v, w)] = i++;

  // Set up 'id_to_vertex' and 'vertex_to_id'
  i = 0;
  vertex_to_id.clear();
  for (int v : g)
    vertex_to_id[v] = i++;

  // Initialize a set of scenarios
  std::mt19937 rng(seed);
  scenarios.clear();
  for (size_t i = 0; i < nr_scenarios; i++)
    scenarios.push_back(scenario(g, vertex_to_id, edge_to_id, p, rng));
}

double influence_gain(const std::vector<scenario> &scenarios, int v) {
  int reachable_gain = 0;
  for (const scenario &s : scenarios)
    reachable_gain += s.reachable_gain(v);
  return double(reachable_gain) / double(scenarios.size());
}

std::unordered_map<int, double>
influence_gains_slow(const std::vector<scenario> &scenarios) {
  std::unordered_map<int, double> inf_gains;
  for (int v : scenarios[0].get_digraph())
    inf_gains[v] = influence_gain(scenarios, v);
  return inf_gains;
}

std::unordered_map<int, double>
influence_gains(const std::vector<scenario> &scenarios) {
  std::unordered_map<int, double> inf_gains;
  for (const scenario &sc : scenarios) {
    std::unordered_map<int, int> reach_gains(sc.all_reachable_gains());
    for (std::pair<int, int> p : reach_gains)
      inf_gains[p.first] += double(p.second) / double(scenarios.size());
  }
  return inf_gains;
}
