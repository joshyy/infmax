#ifndef SCENARIO_H_
#define SCENARIO_H_

#include <map>
#include <random>
#include <stack>
#include <unordered_map>
#include <unordered_set>
#include <utility>

#include "digraph.h"

class scenario {
private:
  const digraph &g;
  const std::map<int, int> &vertex_to_id;
  const std::map<std::pair<int, int>, int> &edge_to_id;

  std::vector<bool> vertex_id_present;
  std::vector<bool> edge_id_present;

  // Calculate the set of vertices, that are reachable from 'v'.
  std::unordered_set<int> reachable(int v) const;

  struct tarjan_node {
    int index;
    int lowlink;
    bool onStack;
  };

  // Helper function for tarjan().
  void tarjan(int v, int &index, std::unordered_map<int, tarjan_node> &nodes,
              std::stack<int> &S,
              std::vector<std::unordered_set<int>> &ccs) const;

  // Run the algorithm of Tarjan to obtain all strongly connected components.
  std::unordered_map<int, int> tarjan() const;

  // Obtain a graph, that has a vertex for each component ('comp') and an edge
  // if there is an edge from a vertex in one component to a vertex in another
  // component.
  digraph reduce(const std::unordered_map<int, int> &comp) const;

public:
  scenario(const digraph &g, const std::map<int, int> &vertex_to_id,
           const std::map<std::pair<int, int>, int> &edge_to_id, double p,
           std::mt19937 &rng);

  // Get the graph, that underlies this scenario.
  const digraph &get_digraph() const;

  // Returns, how many unreached vertices are reachable by 'v'.
  int reachable_gain(int v) const;

  // Returns a map with the pairs (v, reachable_gain(v)) for each vertex v in
  // g.
  std::unordered_map<int, int> all_reachable_gains() const;

  // Remove all vertices from the scenario, that are reachable from 'v'.
  void remove_reachable(int v);
};

#endif // SCENARIO_H_
