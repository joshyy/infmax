#include "bound_set.h"

#include <cassert>

bound_set::iterator::iterator(const bound_set &bs, int pos, int x,
                              base_type mask)
    : bs(bs), pos(pos), x(x), mask(mask) {}
bound_set::iterator &bound_set::iterator::operator++() {
  do {
    x++;
    mask = (mask << 1);
    if (size_t(x) == bs.nr_bits)
      return *this;
  } while (mask != 0 && (bs.bits[pos] & mask) == 0);
  if (mask == 0) {
    pos++;
    while (bs.bits[pos] == 0 && size_t(x) < bs.nr_bits) {
      pos++;
      x += _W;
    }
    if (size_t(x) >= bs.nr_bits) {
      x = bs.nr_bits;
      return *this;
    }
    mask = 1UL;
    while (mask != 0 && (bs.bits[pos] & mask) == 0) {
      x++;
      mask = (mask << 1);
      if (size_t(x) == bs.nr_bits)
        return *this;
    }
    assert(mask != 0);
  }
  return *this;
}
bound_set::iterator bound_set::iterator::operator++(int) {
  iterator old(*this);
  operator++();
  return old;
}
const int &bound_set::iterator::operator*() const { return x; }
bool operator==(const bound_set::iterator &lhs,
                const bound_set::iterator &rhs) {
  return (&lhs.bs == &rhs.bs) && (lhs.x == rhs.x);
}
bool operator!=(const bound_set::iterator &lhs,
                const bound_set::iterator &rhs) {
  return !operator==(lhs, rhs);
}

bound_set::bound_set() : nr_bits(0), nr_elements(0), bits() {}
bound_set::bound_set(int max_size)
    : nr_bits(max_size), nr_elements(0), bits((max_size + _W - 1) / _W, 0) {}

size_t bound_set::size() const { return nr_elements; }

#define MASK01 (0x5555555555555555UL)
#define MASK02 (0x3333333333333333UL)
#define MASK04 (0x0F0F0F0F0F0F0F0FUL)
#define MASK08 (0x00FF00FF00FF00FFUL)
#define MASK16 (0x0000FFFF0000FFFFUL)
#define MASK32 (0x00000000FFFFFFFFUL)
size_t nr_ones(base_type b) {
  b = (b & MASK01) + ((b & ~MASK01) >> 1);
  b = (b & MASK02) + ((b & ~MASK02) >> 2);
  b = (b & MASK04) + ((b & ~MASK04) >> 4);
  b = (b & MASK08) + ((b & ~MASK08) >> 8);
  b = (b & MASK16) + ((b & ~MASK16) >> 16);
  b = (b & MASK32) + ((b & ~MASK32) >> 32);
  return b;
}
size_t bound_set::calc_size() const {
  size_t size = 0;
  for (base_type b : bits)
    size += nr_ones(b);
  return size;
}

void bound_set::insert(int x) {
  assert(x >= 0 && size_t(x) < nr_bits);
  base_type mask = (1UL << (x % _W));
  size_t pos = x / _W;
  if ((bits[pos] & mask) == 0)
    nr_elements++;
  bits[pos] |= mask;
}
void bound_set::remove(int x) {
  assert(x >= 0 && size_t(x) < nr_bits);
  base_type mask = (1UL << (x % _W));
  size_t pos = x / _W;
  if ((bits[pos] & mask) != 0)
    nr_elements--;
  bits[pos] &= ~mask;
}
bool bound_set::is_element(int x) const {
  assert(x >= 0 && size_t(x) < nr_bits);
  base_type mask = (1UL << (x % _W));
  size_t pos = x / _W;
  return (bits[pos] & mask) != 0;
}
bound_set::iterator bound_set::begin() const {
  iterator it(*this, 0, 0, 1UL);
  if (!is_element(0))
    it++;
  return it;
}
bound_set::iterator bound_set::end() const {
  return iterator(*this, nr_bits / _W, nr_bits, 1UL << (nr_bits % _W));
}
void bound_set::unify(const bound_set &s) {
  assert(nr_bits == s.nr_bits);
  for (size_t i = 0; i < bits.size(); i++)
    bits[i] |= s.bits[i];
  nr_elements = calc_size();
}
void bound_set::intersect(const bound_set &s) {
  assert(nr_bits == s.nr_bits);
  for (size_t i = 0; i < bits.size(); i++)
    bits[i] &= s.bits[i];
  nr_elements = calc_size();
}
void bound_set::complement() {
  for (base_type &b : bits)
    b = ~b;
  nr_elements = nr_bits - nr_elements;
}

std::ostream &operator<<(std::ostream &ostm, const bound_set &bs) {
  for (size_t x = 0; x < bs.nr_bits; x++)
    if (bs.is_element(x))
      ostm << x << " ";
  return ostm;
}
