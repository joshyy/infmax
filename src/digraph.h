#ifndef DIGRAPH_H_
#define DIGRAPH_H_

#include <cassert>
#include <istream>
#include <unordered_map>
#include <unordered_set>
#include <utility>
#include <vector>

// Reads all edges from a stream with one edge per line (vertices seperated
// by a whitespace) and returns a vector of pairs. Each pair represents an
// edge.
std::vector<std::pair<int, int>> edges_from_stream(std::istream &stm);

class digraph {
private:
  // Number of edges
  int m;

  // A number, that is larger than the identifiers of all vertices in the
  // digraph
  int next_free_vertex;

  // Set of the vertices in the digraph
  std::unordered_set<int> vertices;

  // Sets of neighbors for each vertex
  std::unordered_map<int, std::unordered_set<int>> out;
  std::unordered_map<int, std::unordered_set<int>> in;

  // Returns true, if all of the stored data is consistent and false otherwise.
  bool is_consistent() const;

public:
  // ##########################################################################
  // Constructors                                                           ###
  // ##########################################################################

  // Initializes the digraph without any vertices and edges.
  digraph();

  // Initializes the digraph from another digraph.
  digraph(const digraph &g);

  // Initializes the digraph with n vertices with no edges in between. The
  // identifiers of the vertices are given by {0, 1, ..., n-1}.
  digraph(int n);

  // Initializes the digraph with n vertices (as for digraph(int n)) and adds
  // edges as given by 'edges'. Double edges are ignored.
  digraph(const std::vector<std::pair<int, int>> &edges);

  // ##########################################################################
  // Graph Manipulation                                                     ###
  // ##########################################################################

  // Adds an edge between the nodes 'v' and 'w'. Result is undefined, if one of
  // the vertices does not exist or the edge is already present.
  void add_edge(int v, int w);

  // Adds the vertex 'v' to the digraph. Result is undefined, if the vertex
  // already exists.
  void add_vertex(int v);

  // Removes the edge between 'v' and 'w'. Result is undefined, if one of the
  // vertices does not exist or the edge is not present.
  void rem_edge(int v, int w);

  // Removes the vertex 'v' and all edges 'v' is adjacent to. Result is
  // undefined, if the vertex does not exist.
  void rem_vertex(int v);

  // ##########################################################################
  // Obtaining Information                                                  ###
  // ##########################################################################

  // Returns the number of vertices in the digraph.
  int nr_vertices() const;

  // Returns the number of edges in the digraph.
  int nr_edges() const;

  // Returns true, if 'v' is a vertex of the digraph and false otherwise.
  bool is_vertex(int v) const;

  // Returns an integer, that can be added as vertex (i.e.
  // is_vertex(free_vertex()) always evaluates to false).
  int free_vertex() const;

  // Returns the out-degree of the vertex 'v'. Result is undefined, if the
  // vertex does not exist.
  int deg_out(int v) const;

  // Returns the in-degree of the vertex 'v'. Result is undefined, if the vertex
  // does not exist.
  int deg_in(int v) const;

  // Returns true, if 'v' and 'w' are adjacent (i.e. there goes an edge from
  // 'v' to 'w'), and false otherwise. Result is undefined, if one of the
  // vertices does not exist.
  bool adj(int v, int w) const;

  // Return iterators to iterate through all vertices of the digraph. Iterators
  // are valid until a change to the digraph is made.
  std::unordered_set<int>::const_iterator begin() const;
  std::unordered_set<int>::const_iterator end() const;

  // Returns the set of neighbors for the vertex 'v'. A given reference is
  // valid until the digraph is changed.
  const std::unordered_set<int> &neighbors_out(int v) const;
  const std::unordered_set<int> &neighbors_in(int v) const;

  // Returns a set with all vertices, that are reachable from 'v' (including
  // 'v').
  std::unordered_set<int> reachable(int v) const;
};

#endif // DIGRAPH_H_
