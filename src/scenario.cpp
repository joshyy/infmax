#include "scenario.h"

#include "bound_set.h"

#include <queue>

scenario::scenario(const digraph &g, const std::map<int, int> &vertex_to_id,
                   const std::map<std::pair<int, int>, int> &edge_to_id,
                   double p, std::mt19937 &rng)
    : g(g), vertex_to_id(vertex_to_id), edge_to_id(edge_to_id),
      vertex_id_present(vertex_to_id.size(), true),
      edge_id_present(edge_to_id.size()) {
  std::bernoulli_distribution dist(p);
  for (size_t i = 0; i < edge_id_present.size(); i++)
    edge_id_present[i] = dist(rng);
}

std::unordered_set<int> scenario::reachable(int v) const {
  assert(g.is_vertex(v));
  if (!vertex_id_present[vertex_to_id.at(v)])
    return std::unordered_set<int>();
  std::unordered_set<int> r;
  std::queue<int> next;
  r.insert(v);
  next.push(v);
  while (!next.empty()) {
    int w = next.front();
    next.pop();
    for (int n : g.neighbors_out(w))
      if (r.count(n) == 0 && vertex_id_present[vertex_to_id.at(n)] &&
          edge_id_present[edge_to_id.at(std::make_pair(w, n))]) {
        r.insert(n);
        next.push(n);
      }
  }
  return r;
}

int scenario::reachable_gain(int v) const { return reachable(v).size(); }
void scenario::remove_reachable(int v) {
  std::unordered_set<int> r(reachable(v));
  for (int v : r)
    vertex_id_present[vertex_to_id.at(v)] = false;
}

const digraph &scenario::get_digraph() const { return g; }

void scenario::tarjan(int v, int &index,
                      std::unordered_map<int, tarjan_node> &nodes,
                      std::stack<int> &S,
                      std::vector<std::unordered_set<int>> &ccs) const {
  nodes[v].index = index;
  nodes[v].lowlink = index;
  index++;
  S.push(v);
  nodes[v].onStack = true;

  for (int w : g.neighbors_out(v))
    if (vertex_id_present[vertex_to_id.at(w)] &&
        edge_id_present[edge_to_id.at(std::make_pair(v, w))]) {
      if (nodes[w].index == -1) {
        tarjan(w, index, nodes, S, ccs);
        nodes[v].lowlink = std::min(nodes[v].lowlink, nodes[w].lowlink);
      } else if (nodes[w].onStack) {
        nodes[v].lowlink = std::min(nodes[v].lowlink, nodes[w].index);
      }
    }

  if (nodes[v].lowlink == nodes[v].index) {
    std::unordered_set<int> cc;
    int w;
    do {
      w = S.top();
      S.pop();
      nodes[w].onStack = false;
      cc.insert(w);
    } while (w != v);
    ccs.push_back(cc);
  }
}

std::unordered_map<int, int> scenario::tarjan() const {
  std::unordered_map<int, tarjan_node> nodes;
  for (int v : g)
    if (vertex_id_present[vertex_to_id.at(v)])
      nodes[v] = tarjan_node{-1, -1, false};
  int index = 0;
  std::stack<int> S;
  std::vector<std::unordered_set<int>> ccs;
  for (int v : g)
    if (nodes[v].index == -1 && vertex_id_present[vertex_to_id.at(v)])
      tarjan(v, index, nodes, S, ccs);

  std::unordered_map<int, int> component;
  for (size_t i = 0; i < ccs.size(); i++)
    for (int w : ccs[i])
      component[w] = i;
  return component;
}

digraph scenario::reduce(const std::unordered_map<int, int> &comp) const {
  digraph red;
  for (std::pair<int, int> p : comp)
    if (!red.is_vertex(p.second))
      red.add_vertex(p.second);
  for (int v : g)
    if (vertex_id_present[vertex_to_id.at(v)])
      for (int w : g.neighbors_out(v))
        if (vertex_id_present[vertex_to_id.at(w)] &&
            edge_id_present[edge_to_id.at(std::make_pair(v, w))] &&
            !red.adj(comp.at(v), comp.at(w)) && comp.at(v) != comp.at(w))
          red.add_edge(comp.at(v), comp.at(w));
  return red;
}

void dag_dfs(const digraph &g, int v, std::unordered_map<int, bound_set> &r,
             std::unordered_map<int, bool> &handled) {
  if (handled[v])
    return;
  for (int w : g.neighbors_out(v)) {
    dag_dfs(g, w, r, handled);
    r[v].unify(r[w]);
  }
  handled[v] = true;
}

std::unordered_map<int, int> scenario::all_reachable_gains() const {
  std::unordered_map<int, int> components(tarjan());
  digraph red(reduce(components));

  std::unordered_map<int, bound_set> r;
  std::unordered_map<int, bool> handled;
  for (int v : red) {
    r[v] = bound_set(red.nr_vertices());
    r[v].insert(v);
    handled[v] = false;
  }
  for (int v : red)
    dag_dfs(red, v, r, handled);

  std::vector<int> comp_size(red.nr_vertices(), 0);
  for (std::pair<int, int> p : components)
    comp_size[p.second]++;

  std::unordered_map<int, int> nr_reachable;
  for (int v : red) {
    nr_reachable[v] = 0;
    for (int w : r[v])
      nr_reachable[v] += comp_size[w];
  }

  std::unordered_map<int, int> reachable_gains;
  for (int v : g)
    reachable_gains[v] = nr_reachable[components[v]];

  return reachable_gains;
}
