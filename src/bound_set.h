#ifndef BOUND_SET_H_
#define BOUND_SET_H_

#include <cstddef>
#include <iterator>
#include <ostream>
#include <vector>

typedef unsigned long int base_type;
#define _W (sizeof(base_type) * 8)
class bound_set {
private:
  size_t nr_bits;
  size_t nr_elements;

  std::vector<base_type> bits;

  size_t calc_size() const;

public:
  class iterator {
  public:
    const bound_set &bs;
    int pos, x;
    base_type mask;

  public:
    iterator(const bound_set &bs, int pos, int x, base_type mask);
    iterator &operator++();
    iterator operator++(int);
    const int &operator*() const;
    friend bool operator==(const iterator &lhs, const iterator &rhs);
    friend bool operator!=(const iterator &lhs, const iterator &rhs);
  };

  bound_set();
  bound_set(int max_size);

  size_t size() const;

  void insert(int x);
  void remove(int x);
  bool is_element(int x) const;
  iterator begin() const;
  iterator end() const;
  void unify(const bound_set &s);
  void intersect(const bound_set &s);
  void complement();

  friend std::ostream &operator<<(std::ostream &ostm, const bound_set &bs);
};

#endif // BOUND_SET_H_
