#ifndef Q_NODE_H_
#define Q_NODE_H_

#include <ostream>

struct Q_node {
  int vertex;
  double mg1;

  Q_node();
  Q_node(int u, double mg1);

  void clear();

  bool operator<(const Q_node &b) const;
  bool operator<=(const Q_node &b) const;
  bool operator>(const Q_node &b) const;
  bool operator>=(const Q_node &b) const;
  bool operator==(const Q_node &b) const;
  bool operator!=(const Q_node &b) const;

  friend std::ostream &operator<<(std::ostream &str, const Q_node &q);
};

#endif // Q_NODE_H_
