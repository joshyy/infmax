#include "digraph.h"

#include <algorithm>
#include <limits>
#include <queue>
#include <sstream>

// ############################################################################
// Reading the adjacency matrix from an instream                            ###
// ############################################################################

std::string next_line(std::istream &stm) {
  std::string line{""};
  do
    getline(stm, line);
  while ((line[0] == 'c' || line[0] == 'C' || line[0] == '#' || line == "") &&
         stm);
  return line;
}

std::vector<std::pair<int, int>> edges_from_stream(std::istream &stm) {
  std::string line, tmp;
  std::stringstream ss;

  std::vector<std::pair<int, int>> edges;

  while (stm.peek() != EOF) {
    int v1, v2;
    line = next_line(stm);
    ss = std::stringstream(line);
    ss >> v1 >> v2;
    edges.push_back(std::make_pair(v1, v2));
  }

  return edges;
}

// ############################################################################
// Checking digraph consistency                                             ###
// ############################################################################

bool digraph::is_consistent() const {
  // 'vertices' is the set of vertices.
  for (int v : vertices)
    if (out.count(v) == 0)
      return false;
  for (int v : vertices)
    if (in.count(v) == 0)
      return false;
  for (const std::pair<int, std::unordered_set<int>> &p : out)
    if (vertices.count(p.first) == 0)
      return false;
  for (const std::pair<int, std::unordered_set<int>> &p : in)
    if (vertices.count(p.first) == 0)
      return false;

  // All neighbors are vertices.
  for (const std::pair<int, std::unordered_set<int>> &p : out)
    for (int v : p.second)
      if (!is_vertex(v))
        return false;
  for (const std::pair<int, std::unordered_set<int>> &p : in)
    for (int v : p.second)
      if (!is_vertex(v))
        return false;

  // 'm' is the number of edges.
  int _m = 0;
  for (const std::pair<int, std::unordered_set<int>> &p : out)
    _m += p.second.size();
  if (_m != m)
    return false;
  _m = 0;
  for (const std::pair<int, std::unordered_set<int>> &p : in)
    _m += p.second.size();
  if (_m != m)
    return false;

  // 'next_free_vertex' is larger than the identifiers of all vertices
  for (int v : vertices)
    if (next_free_vertex <= v)
      return false;

  return true;
}

// ############################################################################
// Constructors                                                             ###
// ############################################################################

digraph::digraph() : m(0), next_free_vertex(0), vertices(), out(), in() {}

digraph::digraph(const digraph &g)
    : m(g.m), next_free_vertex(g.next_free_vertex), vertices(g.vertices),
      out(g.out), in(g.in) {}

digraph::digraph(int n)
    : m(0), next_free_vertex(n), vertices(n), out(n), in(n) {
  for (int i = 0; i < n; i++)
    add_vertex(i);
  assert(is_consistent());
}

digraph::digraph(const std::vector<std::pair<int, int>> &edges) : digraph() {
  for (std::pair<int, int> e : edges) {
    if (e.first >= next_free_vertex)
      next_free_vertex = e.first + 1;
    if (e.second >= next_free_vertex)
      next_free_vertex = e.second + 1;
    vertices.insert(e.first);
    vertices.insert(e.second);
    if (out[e.first].count(e.second) == 0)
      m++;
    out[e.first].insert(e.second);
    out[e.second];
    in[e.second].insert(e.first);
    in[e.first];
  }
  assert(is_consistent());
}

// ############################################################################
// Graph Manipulation                                                       ###
// ############################################################################

void digraph::add_edge(int v, int w) {
  assert(out.count(v) == 1);
  assert(in.count(w) == 1);
  assert(out[v].count(w) == 0);
  assert(in[w].count(v) == 0);
  out[v].insert(w);
  in[w].insert(v);
  m++;
}

void digraph::add_vertex(int v) {
  assert(vertices.count(v) == 0);
  assert(out.count(v) == 0);
  assert(in.count(v) == 0);
  vertices.insert(v);
  out[v];
  in[v];
  assert(v < std::numeric_limits<int>::max());
  if (next_free_vertex <= v)
    next_free_vertex = v + 1;
}

void digraph::rem_edge(int v, int w) {
  assert(out.count(v) == 1);
  assert(in.count(w) == 1);
  assert(out[v].count(w) == 1);
  assert(in[w].count(v) == 1);
  out[v].erase(w);
  in[w].erase(v);
  m--;
}

void digraph::rem_vertex(int v) {
  assert(vertices.count(v) == 1);
  assert(out.count(v) == 1);
  assert(in.count(v) == 1);
  m -= deg_out(v);
  m -= deg_in(v);
  for (int n : neighbors_out(v))
    if (n != v)
      in[n].erase(v);
  for (int n : neighbors_in(v))
    if (n != v)
      out[n].erase(v);
  vertices.erase(v);
  out.erase(v);
  in.erase(v);
}

// ############################################################################
// Obtaining Information                                                    ###
// ############################################################################

int digraph::nr_vertices() const { return vertices.size(); }

int digraph::nr_edges() const { return m; }

bool digraph::is_vertex(int v) const { return vertices.count(v) == 1; }

int digraph::free_vertex() const { return next_free_vertex; }

int digraph::deg_out(int v) const {
  assert(vertices.count(v) == 1);
  assert(out.count(v) == 1);
  return out.at(v).size();
}

int digraph::deg_in(int v) const {
  assert(vertices.count(v) == 1);
  assert(in.count(v) == 1);
  return in.at(v).size();
}

bool digraph::adj(int v, int w) const {
  assert(vertices.count(v) == 1);
  assert(out.count(v) == 1);
  assert(vertices.count(w) == 1);
  assert(out.count(w) == 1);
  return out.at(v).count(w) == 1;
}

std::unordered_set<int>::const_iterator digraph::begin() const {
  return vertices.begin();
}
std::unordered_set<int>::const_iterator digraph::end() const {
  return vertices.end();
}

const std::unordered_set<int> &digraph::neighbors_out(int v) const {
  assert(vertices.count(v) == 1);
  assert(out.count(v) == 1);
  return out.at(v);
}

const std::unordered_set<int> &digraph::neighbors_in(int v) const {
  assert(vertices.count(v) == 1);
  assert(in.count(v) == 1);
  return in.at(v);
}

std::unordered_set<int> digraph::reachable(int v) const {
  assert(is_vertex(v));
  std::unordered_set<int> r;
  std::queue<int> next;
  next.push(v);
  while (!next.empty()) {
    int w = next.front();
    next.pop();
    for (int n : neighbors_out(w))
      if (r.count(n) == 0) {
        r.insert(n);
        next.push(n);
      }
  }
  return r;
}
