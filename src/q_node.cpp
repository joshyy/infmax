#include "q_node.h"

Q_node::Q_node() : vertex(0), mg1(0) {}

Q_node::Q_node(int u, double mg1) : vertex(u), mg1(mg1) {}

void Q_node::clear() {
  this->vertex = 0;
  this->mg1 = 0;
}

bool Q_node::operator<(const Q_node &b) const { return this->mg1 < b.mg1; }

bool Q_node::operator<=(const Q_node &b) const { return this->mg1 <= b.mg1; }

bool Q_node::operator>(const Q_node &b) const { return this->mg1 > b.mg1; }

bool Q_node::operator>=(const Q_node &b) const { return this->mg1 >= b.mg1; }

bool Q_node::operator==(const Q_node &b) const { return this->mg1 == b.mg1; }

bool Q_node::operator!=(const Q_node &b) const { return this->mg1 != b.mg1; }

std::ostream &operator<<(std::ostream &str, const Q_node &q) {
  str << "Node: " << q.vertex << std::endl;
  str << "Reach: " << q.mg1 << std::endl;
  return str;
}
